## ETAT

### Created by Jordan Hitz at Agilent Technologies on 05.01.2019

Error Type Analysis Tool (__ETAT__) produces CSVs and PNG plots concerning position specific error analysis of BAM file(s).

#### Mandatory arguments:
	* BAM(s)
		* Pathes to BAM file(s). File must end in ".bam" to be recognized.
		* Text file of BAM file(s). File must end in ".txt" or use -b|--bam arguments.
		* A super directory within which all BAMs are saved. Path must end in "/" or use -d|--directory arguments.
	* Reference
		* Use -f|--fasta argument to point to FASTA file
		* The file will be recognized automatically if reference ends in either \*.fa, \*.fasta, or \*.fna
	* Region
		* Specify region to be analyzed. Must be the same as FASTA header and BAM region. Use -r|--region argument to specify.
#### Other
	* -o|--output Specify output directory [current directory]
	* -p|--prefix Specify file output prefix [first BAM file prefix]
	* -t|--threads Processors to utilize [3]
	* -h|--help Outputs available arguments and gives an example before exiting pipeline
	* \* Outputs "Option $1 is not recognized\n" and exits

#### Output:
	* CSVs and PNGs in specified directory (see "-o" argument).
	
#### Software:
	* Bash
	* Perl
	* Rscript
	* Samtools
	* Fastahack
	* Custom Scripts:
		* erarl.V4.bySample.pl
		* BAM_Quality6.pl
		* Run_Analysis3.r
		* ErrorByType.r
		* ErrorByCycle.r
		* plotErrorTypeContext.r
		* plotErrorTypebyCycleContent.r