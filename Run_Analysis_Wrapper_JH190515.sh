#!/bin/bash

#Variables
BAMS=()
#Defaults
DIR=`pwd`
FASTA="/mnt/code/Production/Mapping/tmap_ref_seqs/phix_e_coli/phix_e_coli.fasta";
CHROM='gi|49175990|ref|NC_000913.2|';
THR=3
#Software
#Note: some perl scripts use ${PATH}fastahack and this script uses sed and case
PERL='perl'
SAM='samtools'
R='Rscript'
ERARL="${PERL} /mnt/code/Development/msykes/lighthousebioinformatics/lia2-runner/erarl.V4.bySample.pl"
BAMQ="${PERL} /mnt/code/Development/Jordan/BAM_Quality6.pl"
R4BAMQ="${R} /mnt/code/Development/Jordan/Run_Analysis3.r"
R4ERARL_ByType="${R} /mnt/code/Development/Jordan/ErrorByType.r"
R4ERARL_ByCycle="${R} /mnt/code/Development/Jordan/ErrorByCycle.r"

usage(){
sed -n '/case\s$1/,/esac/p' $0 | perl -ne 'BEGIN{$spacer="                       ";}if($_=~/(\S+)\)\s+#\s+(.+)$/){printf "%s%s%s\n",$1,substr($spacer,length($1)),$2;}'
}

#Poor man getopts
for i in "$@"; do
	case $1 in 
		*.bam) # Run on BAM file(s) directly 
			BAMS+=($1) ; shift ;;
		*.txt) # Provide a list of BAM files
			LIST=$1 ; shift ;;
		*/) # Provide a directory underwhich bam files are saved with '*.sorted.bam' extension
			BAM_DIR=$1 ; shift ;;
		-f|--fasta) # Provide a reference file in FASTA format [phix_e_coli.fasta]
			shift; FASTA=$1 ; shift ;;
		*.fa|*.fasta|*.fna) # Provide a reference file in FASTA format [phix_e_coli.fasta]
			FASTA=$1 ; shift ;;
		-r|--region) # Specify a region to analyze as derived from FASTA and BAM [gi|49175990|ref|NC_000913.2|]
			shift; CHROM=$1 ; shift ;;
		-o|--output) # Specify output directory [current directory]
			shift ; DIR=$1 ; DIR=${DIR%/}; shift ;;
		-p|--prefix) # Specify file output prefix [first BAM file prefix]
			shift ; PRE=$1 ; shift ;;
		-t|--threads) # Processors to utilize [3]
			shift ; THR=$1 ; shift ;;
		-h|--help)
			usage; echo -e "Example:\n\nbash $0 file.bam $FASTA -r ${CHROM}\n" ; exit ;;
		*)
			echo -e "Option $1 is not recognized\n"; exit ;;
	esac
done

#Default prefix
if [[ -z ${PRE} ]]; then
	if [[ -n ${LIST} ]]; then
		PRE=${LIST%_bams.txt}
		PRE=${PRE%.txt}
		for BAM in $(cat ${LIST}); do BAMS+=($BAM); done
	elif [[ -n ${BAM_DIR} ]]; then
		if [[ ${BAM_DIR} == *mapping* ]]; then 
			PRE=${BAM_DIR%/mapping*}
			PRE=${PRE##*/}
			BAMS=($(find ${BAM_DIR} -name '*.sorted.bam'))
		else
			BAMS=($(find ${BAM_DIR} -iname '*.bam'))
			PRE=${BAMS[0]%.bam}
			PRE=${PRE%.sorted}
		fi
	else
		PRE=${BAMS[0]%.bam}
		PRE=${PRE%.sorted}
	fi
	PRE=${PRE##*/}
fi

OUT=${DIR}/${PRE}
if [[ ! -d ${OUT} ]]; then mkdir ${OUT}; fi


for B in ${BAMS[@]}; do ${SAM} view -F 2052 ${B} ${CHROM} ; done | tee >(${ERARL} --sam STDIN --out ${OUT}/${PRE}) >(${BAMQ} -f $FASTA | tee >(bgzip -c> ${DIR}/${PRE}_RA.csv.gz) | ${R4BAMQ} ${PRE} ) | perl -F"\t" -ane 'BEGIN{$reads=0;$int=100000;$lvl=1;}++$reads; if($reads >= ($lvl*$int)){printf STDERR "%d reads processed\n",$reads; ++$lvl;}'
${R4ERARL_ByType} ${OUT}/${PRE}.errorByType.csv &
${R4ERARL_ByCycle} ${OUT}/${PRE}.errorByCycle.csv &

echo -e "Finished!\nSee: \n"
ls ${OUT}/${PRE}*